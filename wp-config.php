<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cpg-git');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lIzC5P}SfjP3n]s;8 >^<f<]KJF)y%vu@2IhuVR7<5_>;rr4s9USM]@^!|F96Tzz');
define('SECURE_AUTH_KEY',  ';B==s`Oa@Hh*.Jkk2E)e:7-%3KGB%?=FU*b;b2@? O-i?[tee4K=OkE$w{1bY`]E');
define('LOGGED_IN_KEY',    'da?JT9km4m!.R;yK>Qk]7JUgd^YWkT1y4OSHQ+N/#$ej+;N:+W<<F`{>&3g@u]On');
define('NONCE_KEY',        '}s28v1H>Et:w~[=O$mY-wSqUag1w/,9l=q:3{lFLlRo9fP@;%M{:M[=rkIiJ{C;l');
define('AUTH_SALT',        'z$-)J7Y7JE:T&UM&rIN fT9]%kGq-C[7b)b=D,CM{w832`1>|iY:Ww?;Dgt0_nV9');
define('SECURE_AUTH_SALT', '4Lnn}}Ofj6:eZL7nu(eDa[&~?Y#Zm1X_wxx[qPS-7??{PsEcyHp5n`V5L8G{{Z)N');
define('LOGGED_IN_SALT',   '%e;{%[tPitAJgp6DB[zgLKxNt^/p%Ix_z7=Z%Ugr*/c6Er~r9+,mOA4q%ZA:.ooT');
define('NONCE_SALT',       '}M[;xU^3mA.^ql3*1RP-*YhH)% ?0v1@1)GokoHW:bJ3F4u90&sJxdws2>HlT7|2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
